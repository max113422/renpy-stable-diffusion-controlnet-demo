These are test scripts to demo stable diffusion cgs and bgs in renpy.

1) make sure you have renpy 8+ installed. (Also stable diffusion webui with python 3.10+. Make sure your python can be accessed by cmd)
2) make a new renpy project (note: we used resolution 1280x720, if you use another adjust the params in stable diffusion python script as needed)
3) drop these files in the new project directory accordingly

For poses, feel free to add your own- go wild!
